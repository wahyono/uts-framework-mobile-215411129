// import material flutter
import 'package:flutter/material.dart';

// import class resultScreen
import 'package:uts_framework_mobile/resultScreen.dart';

// import class dataDetails
import 'dataDetails.dart';

// class inputScreen sebagai class yang pertama kali dijalankan
class inputScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyInputScreen(title: 'Input Biodata'),
    );
  }
}

class MyInputScreen extends StatefulWidget {
  const MyInputScreen({Key? key, required this.title, data}) : super(key: key);

  final String title;

  @override
  State<MyInputScreen> createState() => _MyInputScreenState();
}

class _MyInputScreenState extends State<MyInputScreen> {
  // membuat controller input field
  final TextEditingController nama = TextEditingController();
  final TextEditingController nim = TextEditingController();
  final TextEditingController kelas = TextEditingController();
  final TextEditingController nohp = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
          // Center is a layout widget. It takes a single child and positions it
          // in the middle of the parent.
          child: Container(
        margin: new EdgeInsets.all(50),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextFormField(
              controller: nama,

              onFieldSubmitted: (value) {
                setState(() {
                  // ketika tombol submit diklik, variabel nama akan diset value dari TextFormField
                  nama.text = value;
                });
              },

              // obscureText: true,

              // membuat dekorasi TextFormField
              decoration: InputDecoration(
                // suffixIcon: Icon(Icons.search),
                // mengatur placeholder
                labelText: "Masukkan Nama",
                // errorText: '',
                // mengatur warna field input
                fillColor: Colors.white,
                // mengatur input field "fillable"
                filled: true,
                // mengatur warna fokus
                focusColor: Colors.blue,
                // mengatur warna border fokus
                focusedBorder: UnderlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Colors.white, width: 2.0),
                ),
                // mengatur basic border
                enabledBorder: UnderlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Colors.white, width: 2.0),
                ),

                // suffixIcon: Icon(
                //   Icons.error,
                // ),
              ),
            ),
            Container(
              height: 10,
            ),
            TextFormField(
              controller: nim,

              onFieldSubmitted: (value) {
                setState(() {
                  // ketika tombol submit diklik, variabel nim akan diset value dari TextFormField
                  nim.text = value;
                });
              },

              // obscureText: true,

              decoration: InputDecoration(
                // suffixIcon: Icon(Icons.search),
                labelText: "Masukkan NIM",
                // errorText: '',
                fillColor: Colors.white,
                filled: true, // dont forget this line
                focusColor: Colors.blue,
                focusedBorder: UnderlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Colors.white, width: 2.0),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Colors.white, width: 2.0),
                ),

                // suffixIcon: Icon(
                //   Icons.error,
                // ),
              ),
            ),
            Container(
              height: 10,
            ),
            TextFormField(
              controller: kelas,
              onFieldSubmitted: (value) {
                setState(() {
                  // ketika tombol submit diklik, variabel kelas akan diset value dari TextFormField
                  kelas.text = value;
                });
              },

              // obscureText: true,

              decoration: InputDecoration(
                // suffixIcon: Icon(Icons.search),
                labelText: "Masukkan Kelas",
                // errorText: '',
                fillColor: Colors.white,
                filled: true, // dont forget this line
                focusColor: Colors.blue,
                focusedBorder: UnderlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Colors.white, width: 2.0),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Colors.white, width: 2.0),
                ),

                // suffixIcon: Icon(
                //   Icons.error,
                // ),
              ),
            ),
            Container(
              height: 10,
            ),
            TextFormField(
              controller: nohp,

              onFieldSubmitted: (value) {
                setState(() {
                  // ketika tombol submit diklik, variabel nohp akan diset value dari TextFormField
                  nohp.text = value;
                });
              },

              // obscureText: true,

              decoration: InputDecoration(
                // suffixIcon: Icon(Icons.search),
                labelText: "Masukkan No Hp",
                // errorText: '',
                fillColor: Colors.white,
                filled: true, // dont forget this line
                focusColor: Colors.blue,
                focusedBorder: UnderlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Colors.white, width: 2.0),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Colors.white, width: 2.0),
                ),

                // suffixIcon: Icon(
                //   Icons.error,
                // ),
              ),
            ),
            Container(
              height: 40,
            ),
            ElevatedButton(
                onPressed: () {
                  // jika ElevatedButton diklik, navigator akan mengarahkan ke halaman resultScreen dengan membawa data dari variabel nama, nim, kelas, nohp
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => MyResultScreen(
                                data: new DataDetails(
                                    nama.text, nim.text, kelas.text, nohp.text),
                                title: 'Result',
                              )));
                },
                child: Container(
                  // width: 200,
                  child: Text(
                    'Masuk',
                  ),
                ))
          ],
        ),
      )),
    );
  }
}
