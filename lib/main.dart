// import material yang dibutuhkan flutter
import 'package:flutter/material.dart';

// import class inputScreen dari direktori /lib
import 'package:uts_framework_mobile/inputScreen.dart';

void main() {
  // menjalankan class MyApp
  runApp(const MyApp());
}

// class MyApp yang akan dijalankan pertama kali
class MyApp extends StatelessWidget {
  // constructor. pada class ini tidak membutuhkan parameter
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
//   komponen akan di-build menggunakan widget build
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // nama aplikasi ini
      title: 'UTS Framework Mobile Wahyu Putro Pamungkas 215411129',

      //   tema yang digunakan pada aplikasi ini
      theme: ThemeData(
        //   mengatur warna AppBar
        primarySwatch: Colors.blue,
      ),
      //   widget build akan memanggil class MyHomepage dengan passing parameter title
      home: const MyHomePage(title: 'UTS Wahyu'),
    );
  }
}

// class MyHomepage melakukan extends terhadap Stateful widget yang berarti class ini bersifa dinamis
class MyHomePage extends StatefulWidget {
  // constructor class ini mendapatkan variabel title yang dipassing dari action sebelumnya yaitu "title"
  // title mendapatkan atribut required, jadi action sebelumnya harus melakukan passing title
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // inisialisasi variabel title
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    //   widget scaffold adalah root dari seluruh widget
    return Scaffold(
      // AppBar digunakan untuk membuat action bar
      appBar: AppBar(
        //   mengatur judul AppBar
        title: Text(widget.title),
      ),
      //   body adalah seluruh komponen yang ada di bagian bawah dari AppBar
      //   widget Center akan membuat seluruh komponen memiliki alignment "center"
      body: Center(
          // Center is a layout widget. It takes a single child and positions it
          // in the middle of the parent.
          //   widget Center memiliki child dalam bentuk widget COntainer
          child: Container(
        //   mengatur margin container, yaitu 50dp untuk seluruh sisi (top, right, bottom, left)
        margin: new EdgeInsets.all(50),
        // Container memiliki child dalam bentuk widget Column
        child: Column(
          // mengatur aligmnent utama dengan nilai "center"
          mainAxisAlignment: MainAxisAlignment.center,
          //   widget Row digunakan untuk membuat elemen dalam bentuk horizontal
          children: <Widget>[
            Row(
              // mainAxisAlignment: MainAxisAlignment.center,
              children: [
                //   widget Expanded digunakan untuk membuat komponen yang luasnya memenuhi seluruh ruang kosong yang ada
                Expanded(
                    flex: 1,
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Text(
                        'Nama:',
                      ),
                    )),
                Expanded(
                  flex: 2,
                  child: Text(
                    '   Wahyu Putro Pamungkas',
                  ),
                )
              ],
            ),
            // membuat ruang kosong setinggi 10dp
            Container(
              height: 10,
            ),
            // membuat komponen horizontal/baris kedua
            Row(
              // mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                    flex: 1,
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Text(
                        'NIM:',
                      ),
                    )),
                Expanded(
                  flex: 2,
                  child: Text(
                    '   215411129',
                  ),
                )
              ],
            ),
            // membuat baris ketiga
            Container(
              height: 10,
            ),
            Row(
              // mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                    flex: 1,
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Text(
                        'KELAS:',
                      ),
                    )),
                Expanded(
                  flex: 2,
                  child: Text(
                    '   IF K1',
                  ),
                )
              ],
            ),
            Container(
              height: 10,
            ),
            // membuat baris keempat
            Row(
              // mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                    flex: 1,
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Text(
                        'UNIVERSITAS:',
                      ),
                    )),
                Expanded(
                  flex: 2,
                  child: Text(
                    '   Universitas Teknologi \n   Digital Indonesia',
                  ),
                )
              ],
            ),
            Container(
              height: 40,
            ),
            // widget ElevatedButton untuk membuat tombol
            ElevatedButton(
                onPressed: () {
                  // navigator digunakan sebagai link menuju halaman selanjutnya
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => inputScreen()));
                },
                child: Container(
                  // width: 200,
                  child: Text(
                    'Masuk',
                  ),
                ))
          ],
        ),
      )),
    );
  }
}
