import 'package:flutter/material.dart';
import 'package:uts_framework_mobile/inputScreen.dart';

import 'dataDetails.dart';

class MyResultScreen extends StatefulWidget {
  // const MyResultScreen({Key? key, required this.title, required this.data}) : super(key: key);

//   inisialisasi data yang dipasing dari inputScreen
  final DataDetails data;

// inisialisasi title AppBar
  final String title;

  MyResultScreen({required this.data, required this.title});
  @override
  State<MyResultScreen> createState() => _MyResultScreenState();
}

// class ini digunakan untuk menampilkan seluruh data yang dipassing dari class inputScreen
// masing-masing data akan dimasukkan dalam sebuah widget Row yang memiliki child widget Expanded
class _MyResultScreenState extends State<MyResultScreen> {
  // __MyResultScreenState({required this.data})
  // late final DataDetails data;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
          // Center is a layout widget. It takes a single child and positions it
          // in the middle of the parent.
          child: Container(
        margin: new EdgeInsets.all(50),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              // mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                    flex: 1,
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Text(
                        'Nama:',
                      ),
                    )),
                Expanded(
                  flex: 2,
                  child: Text(
                    '   ' + widget.data.nama,
                  ),
                )
              ],
            ),
            Container(
              height: 10,
            ),
            Row(
              // mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                    flex: 1,
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Text(
                        'NIM:',
                      ),
                    )),
                Expanded(
                  flex: 2,
                  child: Text(
                    '   ' + widget.data.nim,
                  ),
                )
              ],
            ),
            Container(
              height: 10,
            ),
            Row(
              // mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                    flex: 1,
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Text(
                        'KELAS:',
                      ),
                    )),
                Expanded(
                  flex: 2,
                  child: Text(
                    '   ' + widget.data.kelas,
                  ),
                )
              ],
            ),
            Container(
              height: 10,
            ),
            Row(
              // mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                    flex: 1,
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Text(
                        'No Hp:',
                      ),
                    )),
                Expanded(
                  flex: 2,
                  child: Text(
                    '   ' + widget.data.nohp,
                  ),
                )
              ],
            ),
            Container(
              height: 40,
            ),

            // ElevatedButton(
            //     onPressed: (){
            //       Navigator.push(context, MaterialPageRoute(builder: (context) => inputScreen()));
            //     },
            //   child: Container(
            //     // width: 200,
            //     child: Text(
            //       'Masuk',
            //     ),
            //   )
            // )
          ],
        ),
      )),
    );
  }
}
